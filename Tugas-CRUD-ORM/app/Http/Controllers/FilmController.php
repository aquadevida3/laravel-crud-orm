<?php

namespace App\Http\Controllers;
use DB;
use App\Film;
use File;
use Illuminate\Http\Request;

class FilmController extends Controller
{
    //ini untuk perintah awal di jalankan agar sebelum login ga bisa akses tombol tambah edit delete di path /film
    public function __construct()
    {
        $this->middleware('auth')->except(['index','show']);

        // $this->middleware('log')->only('index');

        // $this->middleware('subscribed')->except('store');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $film = Film::paginate(3);//klo mau munculin semua Film::All();
        return view ('film.index',compact('film'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $cast = DB::table('cast')->get();   
        return view ('film.create', compact('cast'));
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
          //buat dulu validasi datanya dengan documentasi laravel supaya tidak sembarang input
          $request->validate([
            'judul' => 'required',
            'ringkasan' => 'required',
            'tahun'=> 'required',
            'cast_id'=> 'required',
            'poster'=> 'required|image|mimes:jpeg,png,jpg'
        ]);
          $namaposter = time().'.'.$request->poster->extension();
          $request->poster->move(public_path('poster'),$namaposter);
          //buat manggil model Film
          $film = new Film;
          $film->judul =$request->judul;
          $film->ringkasan =$request->ringkasan;
          $film->tahun =$request->tahun;
          $film->cast_id =$request->cast_id;
          $film->poster =$namaposter;
          $film->save();

          return redirect('/film');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $film = Film::findOrFail($id);
        return view('film.show',compact('film'));        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cast = DB::table('cast')->get();   
        $film = Film::findOrFail($id);
        return view('film.edit',compact('film','cast'));  
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        $request->validate([
            'judul' => 'required',
            'ringkasan' => 'required', 
            'tahun'=> 'required',
            'cast_id'=> 'required',
            'poster'=> '|image|mimes:jpeg,png,jpg'
        ]);
        if($request->has('poster')){
            $film = Film::find($id);     

            $path = 'poster/';                  //ini untuk delete file foto yang lama biar ga numpuk
            File::delete($path . $film->poster);//sampe sini


            $namaposter = time().'.'.$request->poster->extension();
            $request->poster->move(public_path('poster'),$namaposter);

            $film->judul = $request->judul;
            $film->ringkasan = $request->ringkasan;
            $film->tahun = $request->tahun;
            $film->cast_id = $request->cast_id;
            $film->poster = $namaposter;

            $film->save();
            
            return redirect('/film');

        }else{
            $film = Film::find($id); 
            $film->judul = $request->judul;
            $film->ringkasan = $request->ringkasan;
            $film->tahun = $request->tahun;
            $film->cast_id = $request->cast_id;
            
            
            $film->save();

            return redirect('/film');
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
            $film = Film::find($id);     

            $path = 'poster/';                  //ini untuk delete file foto yang lama biar ga numpuk
            File::delete($path . $film->poster);//sampe sini

            $film->delete();

            return redirect('/film');
    }
}
