@extends('layout.master')

@section('judul')
	Halaman edit Kategori {{$profile->user->name}}
@endsection

@section('content')
<form method="post" action="/profil/{{$profile->id}}">
    @csrf
    @method('PUT')
    <div class="form-group">
      <label>Umur User</label>
      <input type="text" value="{{$profile->user->name}}" class="form-control" disabled>
    </div>
    <div class="form-group">
      <label>Email User</label>
      <input type="text" value="{{$profile->user->email}}" class="form-control" disabled>
    </div>
    <div class="form-group">
      <label>Umur Profile</label>
      <input type="number" name="umur" value="{{$profile->umur}}" class="form-control" >
    </div>
    @error('umur')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label>Biodata</label>
      <textarea class="form-control" name="bio">{{$profile->bio}}</textarea>
    </div>
    @error('bio')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label>Alamat</label>
      <textarea class="form-control" name="alamat">{{$profile->alamat}}</textarea>
    </div>
    @error('bio')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Simpan</button>
  </form>
@endsection