<div class="sidebar">
    <!-- Sidebar user (optional) -->
    <div class="user-panel mt-3 pb-3 mb-3 d-flex">
      <div class="image">
        <img src="{{asset('admin/dist/img/user2-160x160.jpg')}}" class="img-circle elevation-2" alt="User Image">
      </div>
      <div class="info">

        @guest
          <a href="#" class="d-block">Anda Belum Login</a>
        @endguest

        @auth
        <a href="#" class="d-block">{{Auth::user()->name}} </a>
        @endauth
      </div>
    </div>

    <!-- SidebarSearch Form -->
    <div class="form-inline">
      <div class="input-group" data-widget="sidebar-search">
        <input class="form-control form-control-sidebar" type="search" placeholder="Search" aria-label="Search">
        <div class="input-group-append">
          <button class="btn btn-sidebar">
            <i class="fas fa-search fa-fw"></i>
          </button>
        </div>
      </div>
    </div>

    <!-- Sidebar Menu -->
    <nav class="mt-2">
      <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <!-- Add icons to the links using the .nav-icon class
             with font-awesome or any other icon font library -->
        <li class="nav-item">
          <a href="/" class="nav-link">
            <i class="nav-icon fas fa-th"></i>
            <p>
                Dashboard
              <span class="right badge badge-danger">New</span>
            </p>
          </a>
        </li>
        <li class="nav-item">
            <a href="/data-tables" class="nav-link">
              <i class="nav-icon fas fa-th"></i>
              <p>
                Data Table
              </p>
            </a>
          </li>
            @auth
          <li class="nav-item">
            <a href="/cast" class="nav-link">
              <i class="nav-icon fas fa-th"></i>
              <p>
                Cast
              </p>
            </a>
          </li>

          <li class="nav-item">
            <a href="/profil" class="nav-link">
              <i class="nav-icon fas fa-user"></i>
              <p>
                Profile
              </p>
            </a>
          </li>

          <a style="text-align: center;" class="nav-link bg-danger" href="{{ route('logout') }}"
            onclick="event.preventDefault();
            document.getElementById('logout-form').submit();">
            <i class="nav-icon fas fa-sign-out" aria-hidden="true"></i>
            <p>
              Logout
            </p>
        </a>
        @endauth

        @guest
         <li class="nav-item">
            <a href="/login" class="nav-link bg-primary">
              <p>
                Login
              </p>
            </a>
          </li>
        @endguest


        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
          @csrf
        </form>
        
      </ul>
    </nav>
    <!-- /.sidebar-menu -->
  </div>